package com.winfor.demopricechecker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

//    TextInputEditText input_scan;

    String sku, title, price, shelf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        input_scan = findViewById(R.id.input_scan);

        Intent intent = new Intent("nlscan.action.SCANNER_TRIG");
        intent.putExtra("SCAN_TIMEOUT", 6);
        intent.putExtra("SCAN_TYPE", 1);
        sendBroadcast(intent);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String scanResult = intent.getStringExtra("SCAN_BARCODE1");
                final String scanStatus = intent.getStringExtra("SCAN_STATE");

                if ("ok".equals(scanStatus)) {
                    if (scanResult.contains("RSV")) {
                        switch (scanResult) {
                            case "RSV0000001":
                                sku = "RSV0000001";
                                title = "Tips & Trik Menghadapi Quarter Life Crisis";
                                price = "Rp 53.000,-";
                                shelf = "XX";

                                break;
                            case "RSV0000002":
                                sku = "RSV0000002";
                                title = "Surat Untuk Bapak";
                                price = "Rp 58.000,-";
                                shelf = "XX";

                                break;
                            case "RSV0000003":
                                sku = "RSV0000003";
                                title = "Untuk Hati Yang Pernah Terluka Karena Rasa Percaya";
                                price = "Rp 57.000,-";
                                shelf = "XX";

                                break;
                            case "RSV0000004":
                                sku = "RSV0000004";
                                title = "Orang Goblok Vs Orang Pintar";
                                price = "Rp 54.000,-";
                                shelf = "XX";

                                break;
                            case "RSV0000005":
                                sku = "RSV0000005";
                                title = "Hidup Tenang Tanpa Drama";
                                price = "Rp 54.500,-";
                                shelf = "XX";

                                break;
                            case "RSV0000006":
                                sku = "RSV0000006";
                                title = "Jatuh Bangun Elon Musk";
                                price = "Rp 54.000,-";
                                shelf = "XX";

                                break;
                        }
                        Intent intent2 = new Intent(MainActivity.this, DetailActivity.class);
                        intent2.putExtra("SKU", sku);
                        intent2.putExtra("TITLE", title);
                        intent2.putExtra("PRICE", price);
                        intent2.putExtra("SHELF", shelf);
                        startActivity(intent2);
                    } else {
                        Snackbar.make(findViewById(R.id.lyt_main), "Barcode not match", BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                }
            }
        }, new IntentFilter("nlscan.action.SCANNER_RESULT"));
    }
}