package com.winfor.demopricechecker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

public class DetailActivity extends AppCompatActivity {

    TextInputEditText input_sku, input_title, input_price, input_shelf;

    String sku, title, price, shelf;

    TextView text_countdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Detail");

        Intent intent = new Intent("nlscan.action.STOP_SCAN");
        sendBroadcast(intent);

        sku = getIntent().getStringExtra("SKU");
        title = getIntent().getStringExtra("TITLE");
        price = getIntent().getStringExtra("PRICE");
        shelf = getIntent().getStringExtra("SHELF");

        input_sku = findViewById(R.id.input_sku);
        input_title = findViewById(R.id.input_title);
        input_price = findViewById(R.id.input_price);
        input_shelf = findViewById(R.id.input_shelf);

        input_sku.setText(sku);
        input_title.setText(title);
        input_price.setText(price);
        input_shelf.setText(shelf);

        text_countdown = findViewById(R.id.text_countdown);
        new CountDownTimer(6000, 1000) {

            public void onTick(long millisUntilFinished) {
                text_countdown.setText("This page will close automatically in " + millisUntilFinished / 1000 + " seconds");
            }

            public void onFinish() {
                finish();
            }

        }.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else
            return true;

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}